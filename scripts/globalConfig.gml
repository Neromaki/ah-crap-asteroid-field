/* PLAYER */
global.turnSpeed = 12;      // The rate of turn (higher = faster)
global.moveRate = 35;       // Speed the ship moves up and down

/* ASTEROIDS */
// Speed
global.asteroidMinSpeed = 25;
global.asteroidMaxSpeed = 50;
// Rotation
global.asteroidMinRotation = -10;
global.asteroidMaxRotation = 10;
// Scale
global.asteroidMinScale = 0.6;
global.asteroidMaxScale = 1.25;

global.levels = ds_map_create();
global.levels[? 0]     = 0.65;
global.levels[? 400]   = 0.55;
global.levels[? 650]   = 0.45;
global.levels[? 800]   = 0.35;
global.levels[? 1000]  = 0.25;
global.levels[? 1250]  = 0.15;
global.levels[? 1500]  = 0.05;

/* STARS */
global.starSpeed = 40;
global.starSpawnRate = 0.05;

global.API_URL = 'http://asteroid.rpff.co.uk';
